<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Using Inheritant</title>
</head>
<body>
<?php 
    class Student{
        // Data Members or Properties
        protected $first_name;
        protected $last_name;

        // Create contstructor
        public function __construct($first_name,$last_name)
        {
            $this->first_name=$first_name;
            $this->last_name=$last_name;
            
        }

        public function SayHello(){
            return $this->first_name. " " . $this->last_name;
        }

    }


    class MathStudent extends Student{

        public function sum_number($first_number,$second_number){
            $sum=$first_number + $second_number;

            return $this->first_name. " " . $this->last_name ." and Sum of number is:". $sum;
        }
    }


    //Create Object Student

    $stu = new Student("Long","Dara");
    // create object

    $stu2= new MathStudent("Keo","Chenda");
    // echo "<h2>". $stu->SayHello() ."</h2>";

?>

<h2><?php echo $stu->SayHello(); ?></h2>
<h2><?php echo $stu2->SayHello(); ?></h2>
<h2><?php echo $stu2->sum_number(3,8); ?></h2>


<h2>
    <?php 
        $obj3=new MathStudent("Poung","Davit");
    ?>
</h2>
<h2><?php echo $obj3->sum_number(4,9); ?></h2>


    
</body>
</html>