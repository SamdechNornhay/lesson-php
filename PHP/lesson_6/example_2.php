<?php
      class Fruit{
        //   Properties
            protected $name;
            protected $price;

        public function __construct($defualt_name,$default_price){
            $this->name=$defualt_name;
            $this->price=$default_price;
        }

        // Methodes  or Functions      
        function set_Data($name,$price){
            $this->name=$name;
            $this->price=$price;
        }

        function get_Data(){
            return $this->name ." : ". $this->price;
        }

      }
        
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- <link href="css/style.css" rel="stylesheet"> -->
    </head>
    <body>
        <h3>Using OOP</h3>

        <?php          

           

            //$obj2=new Fruit();
            // $obj2->name="ស្វាយ";
            // $obj2->price=3;

        
        ?>
        
     <h2>
        <?php 
            $objFruit1=new Fruit("Mango",3);
            echo $objFruit1->get_Data();      
        ?>
     </h2>

     <h2>
        <?php 

        //Assessing Methods in class
        $objFruit1->set_Data("Banana",2);
        echo $objFruit1->get_Data();      
        ?>
     </h2>


     <h2>
        <?php 

        //Assessing Methods in class
        $objFruit1->set_Data("ចេកអំបូង",9);
        echo $objFruit1->get_Data();      
        ?>
     </h2>

    </body>
</html>