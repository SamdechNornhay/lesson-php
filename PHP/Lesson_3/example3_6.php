<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>example Operators PHP7</title>
</head>
<body>
<h1>using PHP7</h1>

    <?php
    $a=(false&&foo());
    $b=(true||foo());
    $c=(false and foo());
    $d=(true or foo());
    //the result of expression (false || true) is assigned to $e
    //Acts like:($e=(false || true))
    $e=false || true;
    
    $f=false or true;
    var_dump($e,$f);

    $g=true && false;

    $h=true and false;

    var_dump($g,$h);

    ?>

</body>
</html>